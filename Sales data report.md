# Sales Data Report

**Jack Zhang (jack.n.nz1@gmail.com)**

The sales data for the period between 01/12/2009 and 09/12/2011 was analysed and the requested outcomes placed below. 

## Important notes on the data

There were a few peculiarities and holes in the data. Most notably, around 23% of the data was missing Customer ID numbers. As seen below, this made analysing the share of revenue by customer difficult. Less important was some items were missing descriptions, but this was a much smaller proportion of the data. Additionally, there were some transactions with strange price and quantity values - the highest price was 38970 and the lowest was -53594.36, with the highest quantity being 80995 and lowest -80995.

The database could be better designed to include an order number or a value indicating whether a transaction was later returned. This would make the analysis of returns much easier, as there is no way of easily doing is currently.

The data also consisted of some entries which appeared to be manual stock adjustments, as they had no customer ID and often no description. When they did have a description, it was along the lines of "check", "damaged", "missing", etc. Implementing a different invoice code and some default options for these events in the inventory system used would improve the data, such as invoice codes indicating damage, missing items, etc. The prices and items for these entries were also often wrong, with a price of zero not reflective of the cost of such damages or missing items.

In addition, historical revenue data would be needed to come up with a more accurate forecast of revenue. Weekly revenue data for 2009 and before would greatly improve the forecast, but if that is not available, revenue data for December over some years prior to 2009 would suffice. 

## Requested graphs

![](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-6-1.png)

![unnamed-chunk-6-2](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-6-2.png)

Overall, the sales volume data looks slightly seasonal with gaps on some days such as during the Christmas and Easter seasons. There are also  days of very high volume as well as days with negative volume. I suspect these are large orders that were cancelled, as some of them are proceeded by days with high volumes.

![unnamed-chunk-7-1](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-7-1.png)

Sales volumes over each week of the year has much less noise and irregularity. When plotted like this, we can see a roughly flat trend of volumes over weeks 0-35 then a clear bump in volumes until the end of  the year. 2010 and 2011 are similar in the data we do have, but to the  eye it seems that 2010 has higher volumes, although they certainly trade blows. The data is too incomplete for 2009 to draw general comparisons.

![unnamed-chunk-8-1](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-8-1.png)

Again, further smoothed presentation and we can see roughly flat volumes until around Aug-Sep where there is a bump in volumes.

![unnamed-chunk-9-1](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-9-1.png)

Although a pie chart is nice and visual, it gets quite crowded with only 10 items. See the plots before for clearer revenue by product graphs.

![unnamed-chunk-9-2](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-9-2.png)

![unnamed-chunk-9-3](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-9-3.png)

![unnamed-chunk-9-4](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-9-4.png)

These plots shows an expected trend, with the highest revenue-generating products and customers having an exponential decay-like pattern. There are a couple things of interest:

1. The highest-revenue products seems to include things like postage fees, which would make sense for an online store, but this might not be very useful. This can be easily removed if needed.
2. A lot of revenue is not linked to a customer ID, which is why the NA column is so high. This is a problem and should be looked into.

![unnamed-chunk-9-5](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-9-5.png)

Weighted average prices don't have the same trend as the sales volumes, and it fluctuates a lot less. There may be some seasonal effect, but it is not strong and we would need more data to tell.

## Cleaning refunds for requests before 12/2009

Initially it was thought that refunds could be easily cleaned if invoice no. was the same across purchases and refunds, with refunds having an extra "C" in front of the number. However, this was not the case and another strategy was needed.

Instead, the refunds were cleaned by removing orders that had invoice number beginning with "C" that had a unique customer ID and stock code combination, i.e. a customer refunding a product that they had never purchased in the records, then removing orders with no earlier record of the same customer ID purchasing the same item at same or lower quantities than was what refunded. This resulted in around 3000 orders being removed. It should be noted that this strategy may miss some refunds if the customer purchased the same item at same or larger quantities again after the data collection began, then refunded the first batch. However, 3000 orders in a database of over a million does not influence the analysis much so this effect was thought to be small. For this reason, I recommended an order number to be implemented, such that purchases and refunds would have the same order number.

## Revenue forecast for Dec 2011

We forecast a revenue of £780,000 in the month of Dec 2011. This includes the 9 days that have already passed and is notably worse than the revenue generated in Dec 2010. Without knowing the existing financial situation of the company (costs, ownership structure, etc) it's impossible to say for sure whether the owner can afford to buy a Ferrari, but seeing as this is a sizeable drop from the same time last year, I would say not to buy the new car.

![](/home/clae/Documents/sales-data-forecasting/sales-data-forecasting_files/figure-html/unnamed-chunk-13-1.png)

This is quite surprising - the revenue from the last 9 days in 2011 is roughly at the same level as in 2009 instead of being similar to 2010, which results in a forecasted revenue similar to 2009! This is quite a dramatic drop-off from the levels seen in 2011 and compared to Dec 2010.

A number of strategies were considered to forecast this number. These methods were mainly constrained by the lack of prior years data, as there is only one complete year's worth of data (2010). As previously stated, historical revenue data would help revenue forecasts greatly.

Three main approaches were considered:

1. Very simple and naive approach - see the portion of revenue generated in the first 9 days of Dec 2010 and 2009 and multiply that by the revenue in the first 9 days of Dec 2011 that we do have data on. This is very easy and only requires the existing data but is also very naive, and we cannot place a value on the amount of uncertainty we have of this value. Due to time constraints, this approach was taken.
2. Simpler approach - Analyse trends over time with time series techniques. For this to be of reasonable accuracy to justify the extra effort, we would need previous years revenue data.
3. Slightly complicated approach - resampling or bootstrapping the previous years data to get an estimate of the mean and variance for this year. This could be done with the existing data but we will run into the same problems as approach 2.

For this data, approaches 1 and 2 are similar since there is only 2 prior years of data. By looking at the above plots, we can see that prior 2011 revenue is around the same as in 2010, which makes approach 1 a rough but fair estimate of the revenue. Approach 2 will yield a similar answer as the time series will be based off of averages of the previous two years also. Obviously this is very naive, but we can improve this by looking at the last nine days of 12/2011 and comparing that to the same 9 days in the previous two years, and create an estimate based on that. The specific method to achieve that leads into approach 2.

The time series approach would not be satisfactorily robust on the given data, as it only has one full year to base any yearly effects off of. For this reason, revenue data prior to the given data would be very helpful. Once we have a few years worth of data, it should yield a better estimate of the expected revenue, along with ways of estimating the uncertainly to create uncertainty bounds for which we believe the true revenue would lie between. From the current data, it seems that a Holt-Winters additive model could be used, but the choice of model would be made after the data is sighted.

Approach 3 is a bit more abstract and would require a bit more effort to code up. It suffers from the same problem as approach 2, as even if we resample with replacement from previous data, the question becomes how many samples should we take? Now we have to tune a hyperparameter which there is no clear and accurate way of tuning. Again, if we get past data, it might make this approach worth it as it will make the number of orders to resample clearer, and we could use the empirical distribution after resampling many times to get the variance for a confidence interval. However, I would use approach 2 if we can access more historical data, as the techniques and results are much more studied and straightforward, while requiring less effort.

## Future work

Due to time constraints, a better prediction method was not chosen, although as discussed, other methods would have also been limited by the data. Most likely approach 2 instead.

With extra time, the plots would have been cleaned. The current colour pallets don't look very pleasing, and there are some other problems, such as having months as numeric, and having colours match different years in different graphs. 

This report is also a bit long and not the nicest. With more time I would have added a title page, moved to Word and used numbered sections and figures.